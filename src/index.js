import React from 'react';
import ReactDOM from 'react-dom';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
import GithubCard from './containers/GithubCard.jsx';
import './styles/index.css';

/* Needed for Material-UI library for onTouchTap*/
injectTapEventPlugin();

const App = () => (
  <MuiThemeProvider>
    <GithubCard />
  </MuiThemeProvider>
);

ReactDOM.render(<App />, document.getElementById('root'));