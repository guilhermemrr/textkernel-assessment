import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import SearchIcon from 'material-ui/svg-icons/action/search';
import FlatButton from 'material-ui/FlatButton';
import { fullWhite } from 'material-ui/styles/colors';
import '../styles/SearchBox.css';

const muiStyle = {
  input: {
    marginLeft: '20px',
    width: '100%',
  },
  button: {
    height: 'auto',
  },
};

class SearchBox extends Component {
  
  state = {
    username: '',
  };
  
  render = () => (
    <Paper className='search-container' zDepth={ 1 }>
        
        <TextField
          hintText='Enter an GitHub username...'
          style={ muiStyle.input }
          underlineShow={ false }
          value={ this.state.username }
          onChange={ (e, username) => this.setState({ username }) }
        />
        
        <FlatButton
          backgroundColor='#833a9b'
          hoverColor='#bc4fe0'
          icon={<SearchIcon color={fullWhite} />}
          style={ muiStyle.button }
          onTouchTap={ () => this.props.requestUser(this.state.username) }
        />

    </Paper>
  );
}

SearchBox.propTypes = {
  requestUser: PropTypes.func.isRequired,
}

export default SearchBox;
