import React from 'react';
import PropTypes from 'prop-types';
import '../styles/InfoBlock.css'

const InfoBlock = ({ number, property, url }) => (
  <div className='container'>
    <a href={ url } target='_blank' rel='noopener noreferrer'>
      <div className='number'>{ number }</div>
      <div className='property'>{ property }</div>
    </a>
  </div>
);

InfoBlock.propTypes = {
  number: PropTypes.number.isRequired,
  property: PropTypes.string.isRequired,
  url: PropTypes.string.isRequired,
};

export default InfoBlock;