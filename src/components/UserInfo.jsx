import React from 'react';
import PropTypes from 'prop-types';
import Paper from 'material-ui/Paper';
import Avatar from 'material-ui/Avatar';
import InfoBlock from './InfoBlock';
import { blue500 } from 'material-ui/styles/colors';
import '../styles/UserInfo.css';

const style = {
  textAlign: 'center',
  display: 'inline-block',
};

const UserInfo = ({ login, name, location, company, publicRepos, publicGists, followers, following, avatarUrl }) => {
  
  return (
    <Paper className='user-container' style={style} zDepth={1}>

      <div className='user-avatar'>
        
        <a href={ `https://github.com/${login}` }  rel='noopener noreferrer' target='_blank'>
          <Avatar 
            src={ avatarUrl }
            backgroundColor={ blue500 }
            size={ 120 }
          />

          <div className='user-name'> { name } </div>
        </a>

        { 
          location && 
            <div className='user-location'>{ location }</div>
        }
        
        {
          company &&
            <div className='user-work'>Works at { company }</div>
        }

        

      </div>

      <div className='user-public-info'>
        <InfoBlock
          number={ publicRepos }
          property={ 'Public Repos' }
          url={ `https://github.com/${login}/repositories` }
        />

        <InfoBlock
          number={ publicGists }
          property={ 'Public Gist' }
          url={ `https://gist.github.com/${login}` }
        />

        <InfoBlock
          number={ followers }
          property={ 'Followers' }
          url={ `https://github.com/${login}/followers` }
        />

        <InfoBlock
          number={ following }
          property={ 'Following' }
          url={ `https://github.com/${login}/following` }
        />
      </div>

    </Paper>
  );
};

UserInfo.propTypes = {
  login: PropTypes.string.isRequired,
  name: PropTypes.string,
  location: PropTypes.string,
  company: PropTypes.string,
  publicRepos: PropTypes.number.isRequired,
  publicGists: PropTypes.number.isRequired,
  followers: PropTypes.number.isRequired,
  following: PropTypes.number.isRequired,
  avatarUrl: PropTypes.string.isRequired,
};

export default UserInfo;