import React, { Component } from 'react';
import UserInfo from '../components/UserInfo';
import SearchBox from '../components/SearchBox';
import '../styles/GithubCard.css';

class GithubCard extends Component {

  state = {
    user: null,
    error: null
  };

  requestUser = (username) => {
    fetch(`https://api.github.com/users/${username}`)
      .then((res) => {
        if(res.status === 404)
          throw new Error('Not found');
        return res.json()
      })
      .then(this.updateUser)
      .catch(this.userNotFound);
  }

  userNotFound = (error) =>
    this.setState({ 
      error: error.message,
      user: null,
    });

  updateUser = ({ login, name, location, company, public_repos, public_gists, followers, following, avatar_url }) => 
    this.setState({
      error: null,
      user: {
        login,
        name,
        location,
        company,
        publicRepos: public_repos,
        publicGists: public_gists,
        followers,
        following,
        avatarUrl: avatar_url,
      },
    });

  render() {
    return (
      <div>

        <SearchBox requestUser={ this.requestUser } />
        
        {
          this.state.error !== null &&
            <div className='error'>{ this.state.error}</div>
        }

        {
          this.state.user !== null &&
            <UserInfo { ...this.state.user} />
        }

      </div>
    );
  }

}

export default GithubCard;

