import React from 'react';
import ReactDOM from 'react-dom';
import GithubCard from '../../containers/GithubCard';
import { shallow } from 'enzyme';

describe('Component: UserInfo', () => { 
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<GithubCard />);
  });

  it('should render without crash', () => {
    expect(wrapper.state('error')).toBe(null);
    expect(wrapper.state('user')).toBe(null);
  });

  it('should match with snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });

  it('should render user', () => {
    const userState = {
      login: 'stevejobs',
      name: 'Steve',
      location: 'Silicon Valey',
      company: 'Aple',
      publicRepos: 0,
      publicGists: 0,
      followers: 200,
      following: 5,
      avatarUrl: 'https://avatars0.githubusercontent.com/u/7724351?v=3',
    };
    
    wrapper.setState({
      error: null,
      user: userState,
    });

    setTimeout(() => {
      expect(wrapper.contains('UserInfo')).toBeTruthy();

      expect(wrapper.find('UserInfo').props().login).toEqual(userState.login);
      expect(wrapper.find('UserInfo').props().name).toEqual(userState.name);
      expect(wrapper.find('UserInfo').props().location).toEqual(userState.location);
      expect(wrapper.find('UserInfo').props().company).toEqual(userState.company);
      expect(wrapper.find('UserInfo').props().publicRepos).toEqual(userState.publicRepos);
      expect(wrapper.find('UserInfo').props().publicGists).toEqual(userState.publicGists);
      expect(wrapper.find('UserInfo').props().followers).toEqual(userState.followers);
      expect(wrapper.find('UserInfo').props().following).toEqual(userState.following);
      expect(wrapper.find('UserInfo').props().avatarUrl).toEqual(userState.avatarUrl);
    }, 50);
    

  });

  it('should update to new user', () => {
    const userFromApi = {
      login: 'stevejobs',
      name: 'Steve',
      location: 'Silicon Valey',
      company: 'Aple',
      public_repos: 0,
      public_gists: 0,
      followers: 200,
      following: 5,
      avatar_url: 'https://avatars0.githubusercontent.com/u/7724351?v=3',
    };

    const expectedUserState = {
      login: 'stevejobs',
      name: 'Steve',
      location: 'Silicon Valey',
      company: 'Aple',
      publicRepos: 0,
      publicGists: 0,
      followers: 200,
      following: 5,
      avatarUrl: 'https://avatars0.githubusercontent.com/u/7724351?v=3',
    };

    wrapper.instance().updateUser(userFromApi);
    
    const currentUserState = wrapper.state('user');
    const error = wrapper.state('error');

    expect(error).toBe(null);
    expect(currentUserState).toEqual(expectedUserState);
  });

  it('should render error message if username not found', () => {
    const error = { message: 'Not found' };

    expect(wrapper.contains('.error')).toBeFalsy();

    wrapper.instance().userNotFound(error);    
    setTimeout(() => {
      expect(wrapper.state('error')).toBe('Not found');
      expect(wrapper.contains('.error')).toBeTruthy();
      expect(wrapper.contains('UserInfo')).toBeFalsy();
    }, 50);

  });
  
});