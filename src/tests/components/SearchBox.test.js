import React from 'react';
import ReactDOM from 'react-dom';
import SearchBox from '../../components/SearchBox';
import { shallow } from 'enzyme';

describe('Component: SearchBox', () => { 
  let requestUserSpy;

  beforeEach(() => {
    requestUserSpy = jest.fn();
  });

  it('should render without crash', () => {
    shallow(<SearchBox requestUser={ requestUserSpy } />);
  });

  it('should  match with snapshot', () => {
    const wrapper = shallow(<SearchBox requestUser={ requestUserSpy } />);
    expect(wrapper).toMatchSnapshot();

  });

  it('should call callback passing the username', () => {
    const wrapper = shallow(
      <SearchBox requestUser={ requestUserSpy } />
    );
    const username = 'ribeiroguilherme';

    wrapper.setState({ username: username });
    wrapper.find('FlatButton').simulate('touchTap');

    expect(requestUserSpy).toBeCalledWith(username);
  });
  
});