import React from 'react';
import ReactDOM from 'react-dom';
import UserInfo from '../../components/UserInfo';
import { shallow } from 'enzyme';

describe('Component: UserInfo', () => { 
  let user;

  beforeEach(() => {
    user = {
      login: 'ribeiroguilherme',
      name: 'Guilherme',
      location: 'Athlone, Ireland',
      company: 'Ericsson',
      publicRepos: 5,
      publicGists: 1,
      followers: 2,
      following: 5,
      avatarUrl: 'https://avatars0.githubusercontent.com/u/7724351?v=3',
    };
  });

  it('should render without crash', () => {
    shallow(<UserInfo { ...user } />);
  });

  it('should match with snapshot', () => {
    const wrapper = shallow(<UserInfo { ...user } />);
    expect(wrapper).toMatchSnapshot();
  });

  it('should render user with his props ', () => {
    const wrapper = shallow(<UserInfo { ...user } />);

    expect(wrapper.find('Avatar').props().src).toEqual(user.avatarUrl);
    expect(wrapper.find('.user-location').text()).toEqual(user.location);
    expect(wrapper.find('.user-work').text()).toEqual(`Works at ${user.company}`);
    expect(wrapper.find('InfoBlock').get(0).props.number).toEqual(user.publicRepos);
    expect(wrapper.find('InfoBlock').get(1).props.number).toEqual(user.publicGists);
    expect(wrapper.find('InfoBlock').get(2).props.number).toEqual(user.followers);
    expect(wrapper.find('InfoBlock').get(3).props.number).toEqual(user.following);
  });

  it('should not render location and company divs ', () => {
    const userMissingData = { ...user, company: null, location:null };
    const wrapper = shallow(<UserInfo { ...userMissingData} />);

    expect(wrapper.contains('.user-location')).toBeFalsy();
    expect(wrapper.contains('.user-work')).toBeFalsy();
  });
  
});