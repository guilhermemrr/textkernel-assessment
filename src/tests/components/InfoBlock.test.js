import React from 'react';
import ReactDOM from 'react-dom';
import InfoBlock from '../../components/InfoBlock';
import { shallow } from 'enzyme';

describe('Component: InfoBlock', () => { 
  let url, githubProperty, number;

  beforeEach(() => {
    url = 'https://api.github.com/users/ribeiroguilherme/following';
    githubProperty = 'Following';
    number = 4;
  });

  it('should render without crash', () => {
    shallow(<InfoBlock number={ number } property={ githubProperty } url={ url } />);
  });

  it('should match with snapshot', () => {
    const wrapper = shallow(
      <InfoBlock number={ number } property={ githubProperty } url={ url } />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('should render number and github property', () => {
    const wrapper = shallow(
      <InfoBlock number={ number } property={ githubProperty } url={ url } />
    );
    expect(wrapper.find('.number').text()).toEqual(number.toString());
    expect(wrapper.find('.property').text()).toEqual(githubProperty);
  });

  it('should have an hyperlink over the number and github property', () => {
    const wrapper = shallow(
      <InfoBlock number={ number } property={ githubProperty } url={ url } />
    );

    const hyperlink = wrapper.find('a');
    
    expect(hyperlink.props().href).toEqual(url);
    expect(hyperlink.find('div').length).toEqual(2);
    expect(hyperlink.find('.number').length).toEqual(1);
    expect(hyperlink.find('.property').length).toEqual(1);
  });
});